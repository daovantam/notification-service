FROM openjdk:8
ADD target/notification-service.jar notification-service.jar
EXPOSE 8085
RUN mkdir /images
ENTRYPOINT ["java", "-jar", "notification-service.jar"]