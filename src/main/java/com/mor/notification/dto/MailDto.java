package com.mor.notification.dto;

import java.io.File;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class MailDto {

  private UUID id;
  private String sendTo;
  private String subject;
  private String content;
  private File attach;
}
