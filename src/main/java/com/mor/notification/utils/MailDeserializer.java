package com.mor.notification.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mor.notification.dto.MailDto;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MailDeserializer {

  public MailDto deserialize(byte[] bytes) {
    MailDto mailDto = new MailDto();
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      mailDto =
          objectMapper.readValue(bytes, MailDto.class);
    } catch (IOException e) {
      log.error("MailDeserializer error ", e);
    }
    return mailDto;
  }
}
