package com.mor.notification.service;

import com.mor.notification.dto.MailDto;
import javax.mail.MessagingException;
import org.springframework.stereotype.Service;

@Service
public interface MailService {

  void sendEmailSMTP(MailDto mailDto);

  void sendEmailSMTPWithAttachment(MailDto mailDto) throws MessagingException;
}
