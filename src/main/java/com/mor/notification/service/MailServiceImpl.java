package com.mor.notification.service;

import com.mor.notification.dto.MailDto;
import com.mor.notification.utils.MailDeserializer;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MailServiceImpl implements MailService {

  private final JavaMailSender javaMailSender;
  private final MailDeserializer mailDeserializer;

  @Value("${spring.mail.host}")
  private String mailSender;

  @Autowired
  public MailServiceImpl(@Qualifier("getJavaMailSender") JavaMailSender javaMailSender,
      MailDeserializer mailDeserializer) {
    this.javaMailSender = javaMailSender;
    this.mailDeserializer = mailDeserializer;
  }

  @Override
  public void sendEmailSMTP(MailDto mailDto) {
    SimpleMailMessage message = new SimpleMailMessage();
    log.info("start send email have id {} to {} with subject {} and content {}", mailDto.getId(),
        mailDto.getSendTo(), mailDto.getSubject(), mailDto.getContent());
    message.setFrom(mailSender);
    message.setTo(mailDto.getSendTo());
    message.setSubject(mailDto.getSubject());
    message.setText(mailDto.getContent());
    javaMailSender.send(message);
    log.info("send email have id {} success", mailDto.getId());
  }

  @Override
  public void sendEmailSMTPWithAttachment(MailDto mailDto) throws MessagingException {
    log.info("start send email with attachment have id {}", mailDto.getId());
    MimeMessage message = javaMailSender.createMimeMessage();
    MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);
    messageHelper.setFrom(mailSender);
    messageHelper.setTo(mailDto.getSendTo());
    messageHelper.setSubject(mailDto.getSubject());
    messageHelper.setText("", mailDto.getContent());
    if (mailDto.getAttach() != null) {
      messageHelper.addAttachment(mailDto.getAttach().getName(), mailDto.getAttach());
    }
    javaMailSender.send(message);
    log.info("send email with attachment have id {} success", mailDto.getId());
  }

  @KafkaListener(topics = "emailNoAttach", groupId = "group-id")
  public void listen(byte[] bytes) throws MessagingException {
    MailDto mailDto = mailDeserializer.deserialize(bytes);
    log.info("Email content: {}", mailDto);
    sendEmailSMTPWithAttachment(mailDto);
  }
}
